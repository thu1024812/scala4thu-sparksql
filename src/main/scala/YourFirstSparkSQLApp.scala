import org.apache.spark.SparkConf
import org.apache.spark.sql.{DataFrame, SparkSession}

/**
  * Created by mark on 04/06/2017.
  */
object YourFirstSparkSQLApp extends App{

  val ss = SparkSession
    .builder()
    .master("local[*]")
    .appName("Spark SQL first example")
    .getOrCreate()

  val src: DataFrame =ss.read.option("header", "true")
    .csv("dataset/small-ratings.csv")

  src.createOrReplaceTempView("ratings")
//EX1
  ss.sql("SELECT * FROM ratings ORDER BY timestamp DESC").show()
//EX2
  ss.sql("SELECT movieId,avg(rating) as mean FROM ratings GROUP BY movieId").show()
//EX3
  ss.sql("SELECT movieId,avg(rating) as mean FROM ratings GROUP BY movieId ORDER BY mean DESC").show()
//EX4
  ss.sql("SELECT *  FROM ratings WHERE userId=107799 AND movieId=30707").show()
//  ss.sql("SELECT * FROM ratings").write.json("hi.csv")

  val movieSrc: DataFrame =ss.read.option("header", "true")
    .csv("/Users/mac026/scala4thu-sparksql/dataset/movies.csv")

  movieSrc.createOrReplaceTempView("movies")
//EX5
  ss.sql("SELECT movieId,title FROM movies WHERE movieId=10").show()
//EX6
  ss.sql("SELECT m.movieId,m.title,rating,timestamp\nfrom ratings as r JOIN movies as m WHERE r.movieId=m.movieId\nORDER BY timestamp DESC").show()

}
