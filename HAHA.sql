SELECT movieId,avg(rating) FROM ratings
GROUP BY movieId
ORDER BY avg(rating) DESC
LIMIT 10;

SELECT * FROM ratings;

SELECT movieId,rating FROM ratings;

SELECT COUNT(1) AS size FROM ratings;
-- as 改名稱
SELECT AVG(rating) as mean FROM ratings;

SELECT  movieId,rating FROM  ratings ORDER BY rating;

SELECT * FROM ratings ORDER BY rating ASC;

SELECT * FROM ratings ORDER BY rating ASC ,timestamp DESC;

SELECT min(rating),max(rating) FROM ratings;

SELECT movieId,COUNT(1),avg(rating)  FROM ratings GROUP BY movieId;

SELECT *  FROM ratings WHERE userId=131283;

SELECT *  FROM ratings WHERE userId=131283 AND movieId<10000;

SELECT *  FROM ratings WHERE userId=131283 OR rating>3;

SELECT * FROM ratings as r
  JOIN movies as m
WHERE r.movieId=m.movieId;

SELECT r.movieId,m.title,avg(rating) as mean
FROM ratings AS r JOIN movies as m
WHERE m.movieId=r.movieId
GROUP BY r.movieId	ORDER BY mean DESC
LIMIT 10;

SELECT * FROM ratings ORDER BY timestamp DESC;

SELECT movieId,avg(rating) as mean FROM ratings GROUP BY movieId;
-- (movieId,min,max)

SELECT movieId,min(movieId),max(movieId),avg(rating) as mean FROM ratings GROUP BY movieId;

SELECT movieId,avg(rating) as mean FROM ratings GROUP BY movieId ORDER BY mean DESC;

SELECT *  FROM ratings WHERE userId=107799 AND movieId=30707;

SELECT movieId,title FROM movies WHERE movieId=10;

SELECT m.movieId,m.title,rating,timestamp
from ratings as r JOIN movies as m WHERE r.movieId=m.movieId
ORDER BY timestamp DESC;